// Lesson 3
// function fizzBuzz () {
//     // your code here
//         for (let i = 0; i <= 100; i++) {
//             if((i % 3 === 0) && (i % 5 === 0)){
//                 console.log('FizzBuzz');
//             }else if(i % 3 === 0){
//                 console.log('Fizz');
//             }else if(i % 5 === 0){
//                 console.log('Buzz');
//             }else{
//                 console.log(i);
//             }
//         }
// }

function filterArray (param) {
    // your code here
    const filterParam = param.filter((e) => (!isNaN(parseInt(e)) || Array.isArray(e)));
    const buildArr = []
    filterParam.forEach((e) => {
        if (Array.isArray(e)) {
            e.forEach((item) => {
                if (!buildArr.includes(item)) {
                    buildArr.push(item);
                }
            })
        }
    });
    console.log(buildArr.sort((a, b) => a - b));
}

// Output DON'T TOUCH!
// fizzBuzz()
filterArray([ [2], 23, 'dance', true, [3, 5, 3] ])